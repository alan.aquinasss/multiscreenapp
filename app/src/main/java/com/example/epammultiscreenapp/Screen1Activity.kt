package com.example.epammultiscreenapp

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.epammultiscreenapp.databinding.ActivityScreen1Binding

class Screen1Activity: AppCompatActivity() {
    private val TAG = "Screen1Activity"
    private lateinit var binding: ActivityScreen1Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_screen_1)
        binding.title.text = "Screen 1"

        binding.btnGoForward.setOnClickListener {
            startActivity(Intent(this@Screen1Activity, Screen2Activity::class.java).apply {
                putExtra("currentScreen", "2")
            })
        }
        binding.btnGoBack.setOnClickListener {
            onBackPressed()
        }
        Log.d(TAG, "onCreate")
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")
    }
}